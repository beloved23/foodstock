<?php if ($testmode) { ?>
<div class="text-danger"><b><?php echo $text_testmode; ?></b></div>
<?php } ?>
<form action="index.php?route=payment/ce_pay/callback" method="post">
  <input type="hidden" name="ce_action" value="<?php echo $action ?>" />
  <input type="hidden" name="ce_merchantid" value="<?php echo $mertid; ?>" />
  <input type="hidden" name="ce_amount" value="<?php echo $amount; ?>" />
  <input type="hidden" name="ce_transref" value="<?php echo $transref; ?>" />
  <input type="hidden" name="ce_customerid" value="<?php echo $email; ?>" />
  <input type="hidden" name="ce_notifyurl" value="<?php echo $notify_url; ?>" />
  <input type="hidden" name="ce_window" value="parent"/>
  <input type="hidden" name="ce_data" value="<?php echo $custom; ?>"/>
  <input type="hidden" name="ce_signature" value="<?php echo $signature; ?>"/>
  <?php 
  $memo = '';
  foreach ($products as $product) { 
  $memo .= $product['quantity'].' - '.$product['name'].' - <b>Price</b> ('.number_format($product['price'],2).')<br/>';
  } 
  $memo = '<b>Order Id: '.$oid.'</b><br/>'.$memo;
  ?>
  <input type="hidden" name="ce_memo" value="<?php echo $memo; ?>">
 <div class="buttons">
    <div class="pull-right">
      <input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary"  />
    </div>
  </div>
</form>