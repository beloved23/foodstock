<?php
	$olang = $this->registry->get('language');
	$span = floor(12/$cols);
?>

<?php echo $header; ?>

<div class="container">
  <?php require( PAVO_THEME_DIR."/template/common/config_layout.tpl" ); ?>
  <?php require( PAVO_THEME_DIR."/template/common/breadcrumb.tpl" );  ?>

  <div class="row">
  <?php if( $SPAN[0] ): ?>
	<aside id="sidebar-left" class="col-md-<?php echo $SPAN[0];?>">
	  <?php echo $column_left; ?>
	</aside>	
  <?php endif; ?> 
	
	<div id="sidebar-main" class="col-md-<?php echo $SPAN[1];?>">
      <div id="content" class="box productdeals">
	    <?php echo $content_top; ?>

		<h3 class="hidden"><?php echo $objlang->get('deal_option'); ?></h3>
		<!-- Deal Option -->
		<div class="">
		  <ul class="box-heading nav nav-tabs">
			<?php foreach ($head_titles as $item): ?>
			<?php if ($item['active']): ?>
			<li class="active"><a href="<?php echo $item['href'];?>"><?php echo $item['text'];?></a></li>
			<?php else: ?>
			<li><a href="<?php echo $item['href'];?>"><?php echo $item['text'];?></a></li>
			<?php endif; ?>
			<?php endforeach; ?>
		  </ul>
		</div>

		<!-- Fillter Product -->
		<?php if (count($products) > 0): ?>
		<div class="row product-filter clearfix">
		  <div class="inner">
			
			<div class="filter-right">
			  <div class="sort">
				<span  for="input-sort"><?php echo $objlang->get('text_sort'); ?></span>
				<select id="input-sort" class="form-control" onchange="location = this.value;">
		          <?php foreach ($sorts as $sorts) { ?>
				  <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
				  <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
				  <?php } ?>
				  <?php } ?>
		        </select>
			  </div>

			  <div class="limit">
				<span  for="input-limit"><?php echo $objlang->get('text_limit'); ?></span>
				<select id="input-limit" class="form-control" onchange="location = this.value;">
				  <?php foreach ($limits as $limits) { ?>
				  <?php if ($limits['value'] == $limit) { ?>
				  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
				  <?php } ?>
				  <?php } ?>
				</select>
			  </div>

			  <div class="category">
				<span  for="input-category"><?php echo $objlang->get('text_category'); ?></span>
				<select id="input-category" class="form-control" name="category_id" onchange="location = this.value;">
				  <option value="<?php echo $href_default;?>"><?php echo $objlang->get("text_category_all"); ?></option>
				  <?php foreach ($categories as $category_1) { ?>
				  <?php if ($category_1['category_id'] == $category_id) { ?>
				  <option value="<?php echo $category_1['href']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></option>
				  <?php } ?>
				  <?php foreach ($category_1['children'] as $category_2) { ?>
				  <?php if ($category_2['child_id'] == $category_id) { ?>
				  <option value="<?php echo $category_2['href']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $category_2['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
				  <?php } ?>

				  <?php if (isset($category_2['children'])) { ?>
				  <?php foreach ($category_2['children'] as $category_3) { ?>
				  <?php if ($category_3['child_id'] == $category_id) { ?>
				  <option value="<?php echo $category_3['href']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
				  <?php } else { ?>
				  <option value="<?php echo $category_3['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
				  <?php } ?>
				  <?php } //endforeach categories_2?>
				  <?php } //endif endforeach categories_2?>

				  <?php } //endforeach categories_1?>
				  <?php } //endforeach categories_0?>
				</select>
			  </div>
            </div>
		  </div>
		</div>
		<?php endif; ?>
		
		<!-- List Product -->
		<?php if (count($products) > 0): ?>
		  <div class="box productdeals">
		  	<div class="box-content" >
			  <?php foreach( $products as $i => $product ):  $i=$i+1;?>
				<?php if( $i%$cols == 0 || $cols == 0): ?>
				<div class="row box-product"><?php endif; ?>
				  <div class="product-cols col-sm-<?php echo $span;?> col-xs-12">
							<div class="product-block clearfix">
								<div class="image col-lg-8 col-md-8 no-padding">
									<?php if( $product['special'] ):  ?>
										<div class="product-label-special label">
											<span class="text_sale"><?php echo $objlang->get( 'text_sale' ); ?></span>
											<span class="deal_detail_num"><?php echo $product['deal_discount'];?>%</span>
										</div>
									<?php endif; ?>
									<a href="<?php echo $product['href']; ?>">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
									</a>
								</div>
								<div class="product-meta col-lg-4 col-md-4 no-padding">
									<div class="left">
										<h4 class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
										
										<?php if ($product['price']) { ?>
										<div class="price">
											<?php if (!$product['special']) { ?>
											<?php echo $product['price']; ?>
											<?php } else { ?>
											<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
											<?php } ?>
										</div>
										<?php } ?>

										<?php if ( isset( $product['rating']) ) { ?>
							              	<div class="rating">
								                <?php for ($is = 1; $is <= 5; $is++) { ?>
								                <?php if ($product['rating'] < $is) { ?>
								                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
								                <?php } else { ?>
								                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
								                <?php } ?>
								                <?php } ?>
							              	</div>
							            <?php } ?>
									</div>

									<div class="deal_detail hide">
										<ul>
											<li>
												<span><?php echo $objlang->get("text_discount");?></span>
												<span class="deal_detail_num"><?php echo $product['deal_discount'];?>%</span>
											</li>
											<li>
												<span><?php echo $objlang->get("text_you_save");?></span>
												<span class="deal_detail_num"><span class="price"><?php echo $product["save_price"]; ?></span></span>
											</li>
											<li>
												<span><?php echo $objlang->get("text_bought");?></span>
												<span class="deal_detail_num"><?php echo $product['bought'];?></span>
											</li>
										</ul>
									</div>

									<div class="count_down">										
										<!-- count down -->
										<div class="deal-qty-box hidden">
											<?php echo sprintf($objlang->get("text_quantity_deal"), $product["quantity"]);?>
										</div>

										<div class="item-detail hidden">
											<div class="timer-explain">(<?php echo date($objlang->get("date_format_short"), strtotime($product['date_end_string'])); ?>)</div>
										</div>

										<div id="item<?php echo $module; ?>countdown_<?php echo $product['product_id']; ?>" class="item-countdown"></div>
									</div>

									<div class="right">
										<p class="description"><?php echo $product['description']; ?></p>
										
										<div class="bottom">
											<div class="cart">
												<div class="content-cart">
													<span><?php echo $objlang->get('button_cart'); ?></span>
												</div>
												<button type="button" onclick="cart.addcart('<?php echo $product['product_id']; ?>');" class="btn btn-shopping-cart btn-outline-inverse">
													<i class="fa fa-shopping-cart"></i>
												</button>
											</div>
										</div>
									</div>
									
									<script type="text/javascript">
										jQuery(document).ready(function($){
											$("#item<?php echo $module; ?>countdown_<?php echo $product['product_id']; ?>").lofCountDown({
												formatStyle:2,
												TargetDate:"<?php echo date('m/d/Y G:i:s', strtotime($product['date_end_string'])); ?>",
												DisplayFormat:"<ul><li>%%D%% <div><?php echo $objlang->get("text_days");?></div></li><li> %%H%% <div><?php echo $objlang->get("text_hours");?></div></li><li> %%M%% <div><?php echo $objlang->get("text_minutes");?></div></li><li> %%S%% <div><?php echo $objlang->get("text_seconds");?></div></li></ul>",
												FinishMessage: "<?php echo $objlang->get('text_finish');?>"
											});
										});
									</script>
								</div>
							</div>
						</div>
				<?php if($i%$cols == 0): ?>
				</div><?php endif; ?>
			  <?php endforeach; ?>

			</div><!--end box-content-->
		  </div><!-- end div content list product -->

		  <div class="links"><?php echo $pagination; ?></div>
		<?php endif; ?>		

		<div class="row">
		  <?php if (empty($products)): ?>
		  <div class="col-sm-6 text-left"><?php echo $objlang->get('text_not_empty');?></div>
		  <div class="col-sm-6 text-right">
			<div class="buttons">
			  <div class="pull-right"><a href="<?php echo $objurl->link('common/home'); ?>" class="btn btn-primary"><?php echo $objlang->get('button_continue'); ?></a></div>
			</div>
		  </div>
		  <?php endif; ?>
		</div>

	    <?php echo $content_bottom; ?>
	  </div><!-- end div #content -->
	</div><!-- end sidebar -->

	  <?php if( $SPAN[2] ): ?>
		<aside id="sidebar-right" class="col-md-<?php echo $SPAN[2];?>">	
		  <?php echo $column_right; ?>
		</aside>
	  <?php endif; ?>

  </div><!-- end div .row -->
	
</div><!-- end div .container -->
<?php echo $footer; ?>
