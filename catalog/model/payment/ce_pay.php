<?php 
class ModelPaymentCEPay extends Model { 
  	public function getMethod($address, $total) {
		$this->load->language('payment/ce_pay');		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('ce_pay_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");		
		if ($this->config->get('ce_pay_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('ce_pay_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}	
		$currencies = array(
			'NGN'
		);		
		if (!in_array(strtoupper($this->currency->getCode()), $currencies)) {
			$status = false;
		}								
		$method_data = array();	
		if ($status) {  
      		$method_data = array( 
        		'code'       => 'ce_pay',
        		'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('ce_pay_sort_order')
      		);
    	} 
    	return $method_data;
  	}
	
	public function logOutCall($data) {
		$this->load->model('setting/store');
		$sql = "CREATE TABLE IF NOT EXISTS ". DB_PREFIX ."cashenvoy_outcall (
		  orderid int(11) NOT NULL,
		  transref varchar(20) NOT NULL,
		  amount decimal(15,4) NOT NULL,
		  time datetime NOT NULL,
		  PRIMARY KEY (orderid)
		) ENGINE=InnoDB";
		$this->db->query($sql);
		$chk = $this->db->query("SELECT * FROM information_schema.COLUMNS 
		WHERE TABLE_SCHEMA = '". DB_DATABASE ."' AND TABLE_NAME = '". DB_PREFIX ."order' 
		AND COLUMN_NAME = 'transref'");
		if($chk->num_rows<1){
			$sql2 = "ALTER IGNORE TABLE `". DB_PREFIX ."order` ADD transref VARCHAR( 20 ) NULL DEFAULT NULL, ADD UNIQUE (`transref`)";	
			$this->db->query($sql2);	
		} else {
			$sql3 = "ALTER IGNORE TABLE `". DB_PREFIX ."cashenvoy_outcall` CHANGE `time` `time` DATETIME NOT NULL ";
			$this->db->query($sql3);
		}
		$orderid = $this->encryption->decrypt($this->db->escape($data['ce_data']));
		$sql = "INSERT IGNORE INTO ". DB_PREFIX ."cashenvoy_outcall (orderid, transref, amount, `time`) VALUES 
		('".substr($orderid,0,-12)."','".$this->db->escape($data['ce_transref'])."','".$this->db->escape($data['ce_amount'])."',NOW())";
		$sql2 = "UPDATE `". DB_PREFIX ."order` SET transref='".$this->db->escape($data['ce_transref'])."' 
		WHERE order_id='".substr($orderid,0,-12)."'";
		$this->db->query($sql);
		$this->db->query($sql2);
	}
	
	public function pruneOutCall() {
		$this->load->model('setting/store');
		$sql = "select a.orderid from ". DB_PREFIX ."cashenvoy_outcall a, `". DB_PREFIX ."order` b where a.orderid=b.order_id 
		and b.order_status_id>0 order by a.orderid";
		$query = $this->db->query($sql);
		$q = '';
		for($i=0;$i<count($query->rows);$i++){ $q .= "'".$query->rows[$i]['orderid']."', "; }		
		if($q){ 
			$q = substr($q,0,-2);
			$sql2 = "delete from ". DB_PREFIX ."cashenvoy_outcall where orderid in ($q)";
			$this->db->query($sql2); 
		}
	}	
}
?>