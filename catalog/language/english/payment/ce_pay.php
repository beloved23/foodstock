<?php
// Text
$_['text_title']    = '<b><img src="https://www.cashenvoy.com/resources/cashenvoy-small.png" align="absmiddle" alt="CashEnvoy" title="CashEnvoy"/> (Interswitch, Verve, Mastercard, Visacard, eTranzact, GTBank Internet Banking)</b>';
$_['order_failure']    = '<p style="padding-left:20px;">Your Order was not processed!</p>';
$_['text_reason'] 	= 'REASON';
$_['text_testmode']	= 'Warning: The Payment Gateway is in Sandbox (Test) Mode.';
$_['text_total']	= 'Shipping, Handling, Discounts & Taxes';
?>