<?php 
class ControllerPaymentCEPay extends Controller {
	public function index() { 
		$this->load->language('payment/ce_pay');		
		$data['text_testmode'] = $this->language->get('text_testmode');		    	
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['testmode'] = $this->config->get('ce_pay_test');		
		if (!$this->config->get('ce_pay_test')) {
    			$data['action'] = 'webservice';
  		} else {
			$data['action'] = 'sandbox';
		}
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		if ($order_info) {
			$data['mertid'] = trim($this->config->get('ce_pay_mertid'));
			$data['item_name'] = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');							
			$data['products'] = array();			
			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();
				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['option_value'];	
					} else {
						$filename = $this->encryption->decrypt($option['option_value']);
						
						$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
					}									
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}				
				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'price'    => $this->currency->format($product['price'], $order_info['currency_code'], false, false),
					'quantity' => $product['quantity'],
					'option'   => $option_data,
					'weight'   => $product['weight']
				);
			}	
			$data['discount_amount_cart'] = 0;
			$data['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);//segun
			$data['transref'] = $this->generateTXRef($this->session->data['order_id']);//segun
			$data['oid'] = $this->session->data['order_id'];
			$total = $this->currency->format($order_info['total'] - $this->cart->getSubTotal(), $order_info['currency_code'], false
			, false);
			if ($total > 0) {
				$data['products'][] = array(
					'name'     => $this->language->get('text_total'),
					'model'    => '',
					'price'    => $total,
					'quantity' => 1,
					'option'   => array(),
					'weight'   => 0
				);	
			} else {
				$data['discount_amount_cart'] -= $total;
			}			
			/*$data['currency_code'] = $order_info['currency_code'];
			$data['first_name'] = html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8');	
			$data['last_name'] = html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');	
			$data['address1'] = html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8');	
			$data['address2'] = html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8');	
			$data['city'] = html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8');	
			$data['zip'] = html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8');	
			$data['country'] = $order_info['payment_iso_code_2'];*/
			$data['email'] = $order_info['email'];
			/*$data['invoice'] = $this->session->data['order_id'] . ' - ' . html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8') . ' ' . html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
			$data['lc'] = $this->session->data['language'];*/
			$data['return'] = $this->url->link('checkout/success');
			$data['notify_url'] = $this->url->link('payment/ce_pay/callback', '', 'SSL');
			//$data['notify_url'] = $this->url->link('index.php?route=payment/ce_pay/callback', '', 'SSL');
			$data['custom'] = $this->encryption->encrypt($data['transref']);		
			$sdata = $this->config->get('ce_pay_key').$data['transref'].$data['amount'];	
			$data['signature'] = hash_hmac('sha256', $sdata, $this->config->get('ce_pay_key'), false);
			$data['oid'] = $this->session->data['order_id'];
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ce_pay.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/payment/ce_pay.tpl', $data);
			} else {
				return $this->load->view('default/template/payment/ce_pay.tpl', $data);
			}
		}
	}
	
	public function generateTXRef($id){ 
		$rx = mt_rand(10,99);
		$ref = $id.str_shuffle(date("ymdHi").$rx);
		return $ref;
	}

	public function requery($request){	
		//foreach ($this->request->post as $key => $value) {
		//	$request .= '&' . $key . '=' . urlencode(html_entity_decode($value, ENT_QUOTES, 'UTF-8'));
		//} 
		$this->load->language('payment/ce_pay');
		if (!$this->config->get('ce_pay_test')) { 
			$curl = curl_init('https://www.cashenvoy.com/webservice/?cmd=requery');
		} else {
			$curl = curl_init('https://www.cashenvoy.com/sandbox/?cmd=requery');
		} 
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);	
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);	
		//curl_setopt($curl, CURLOPT_SSLVERSION,1);		
		$response = curl_exec($curl);
		if (!$response) {
			$this->log->write('ce_pay :: CURL failed ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
		}
		curl_close($curl);
		return $response;
	}
	
	public function callback() { 
	restore_error_handler(); 	
	if(@$this->request->post['ce_response']){	
		$this->document->setTitle('Order Status');	
		if (isset($this->request->post['ce_data'])) { 
			$order_id = $this->encryption->decrypt($this->request->post['ce_data']);
		} else {
			$order_id = 0;
		}
		$transref = $order_id;
		$order_id = substr($order_id,0,-12);			
		$this->load->model('checkout/order');				
		$order_info = $this->model_checkout_order->getOrder($order_id);
		$mertid = $this->config->get('ce_pay_mertid');		
		if ($order_info) {
			$key = $this->config->get('ce_pay_key');
			$sdata = $key.$transref.$mertid;
			$sign = hash_hmac('sha256', $sdata, $key, false);
			$request = "mertid=$mertid&transref=$transref&signature=$sign";
			$response = $this->requery($request);
			$response = strip_tags(preg_replace('#(<title.*?>).*?(</title>)#', '$1$2', $response)); 			
			$this->log->write('ce_pay :: Requery REQUEST: ' . $request);
			$this->log->write('ce_pay :: Requery RESPONSE: ' . $response);
			$respdata = explode('-',$response);
			$cnt = count($respdata);
			@$returned_transref = $respdata[0];
			@$returned_status = $respdata[1];
			@$returned_amount = $respdata[2];
			$total = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);	
			$pfx = '';			
			if ($cnt==3) {
				$order_status_id = $this->config->get('config_order_status_id');
				if((float)$total != $returned_amount && $returned_status=='C00') { $returned_status='C05'; $a='Transaction amount '; }
				switch($returned_status) {
					case 'C00':
						$order_status_id = $this->config->get('ce_pay_completed_status_id');
						$pfx = "CashEnvoy transaction successful. <br/>
						Reference: $transref";
						break;
					case 'C04':
						$order_status_id = $this->config->get('ce_pay_denied_status_id');
						$pfx = "CashEnvoy transaction denied. <br/>
						Reason: Insufficient funds. <br/>
						Reference: $transref";
						break;
					case 'C05':
						$order_status_id = $this->config->get('ce_pay_failed_status_id');
						$pfx = "CashEnvoy transaction failed. <br/>
						Reason: ".@$a."Error occurred. Contact support@cashenvoy.com <br/>
						Reference: $transref";
						break;	
					case 'C03':
						$order_status_id = $this->config->get('ce_pay_failed_status_id');
						$pfx = "CashEnvoy transaction failed. <br/>
						Reason: No transaction record. <br/>
						Reference: $transref";
						break;	
					default:
						$order_status_id = $this->config->get('ce_pay_failed_status_id');
						$pfx = "We could not obtain the status of your transaction.<br/>
						Please contact us quoting your transaction reference.<br/>
						Reference: $transref";
						break;															
				}
				if (!$order_info['order_status_id']) {
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id, $pfx, TRUE);
				} else {
					$this->model_checkout_order->addOrderHistory($order_id, $order_status_id, $pfx, TRUE);
				}
			} else {
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
			}			
			if($returned_status=='C00'){ $url = $this->url->link('checkout/success'); header("location: $url");
			} else {
				$this->language->load('payment/ce_pay');						
				$data['breadcrumbs'] = array();			
				$data['heading_title'] = $this->language->get('order_failure');
				$data['text_message'] = "<p style='padding-left:20px;'>We've had problems processing payment for your order.</p>
				<p style='padding-left:20px;'><b>$pfx</b></p>";
				$data['button_continue'] = "continue";
				$data['continue'] = 'index.php?route=account/order';
				
				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
				
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
				}
				/*$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/footer',
					'common/header'
				);*/
				//$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));			
			}
		}
	} elseif(@$this->request->post['ce_notifyurl']) { 
		$this->load->model('payment/ce_pay');
		$this->model_payment_ce_pay->logOutCall($this->request->post);	
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Pay with CashEnvoy</title>
		</head>
		<body onLoad="document.submit2cepay_form.submit()">
		<center style="font-family:\'Century Gothic\';color:#c00;font-size:14px">
			<b>Please wait....</b>
		</center>
		<form method="post" name="submit2cepay_form" action="https://www.cashenvoy.com/'.$this->request->post['ce_action'].'/?cmd=cepay" target="_self"/>
		<input type="hidden" name="ce_merchantid" value="'.$this->request->post['ce_merchantid'].'"/>
		<input type="hidden" name="ce_transref" value="'.$this->request->post['ce_transref'].'"/>
		<input type="hidden" name="ce_amount" value="'.$this->request->post['ce_amount'].'"/>
		<input type="hidden" name="ce_customerid" value="'.$this->request->post['ce_customerid'].'"/>
		<input type="hidden" name="ce_memo" value="'.$this->request->post['ce_memo'].'"/>
		<input type="hidden" name="ce_data" value="'.$this->request->post['ce_data'].'"/>
		<input type="hidden" name="ce_notifyurl" value="'.$this->request->post['ce_notifyurl'].'"/>
		<input type="hidden" name="ce_signature" value="'.$this->request->post['ce_signature'].'"/>
		<input type="hidden" name="ce_window" value="parent"/>
		</form>
		</body></html>';
	} else { 
		$this->load->model('payment/ce_pay');
		//this function still exists in the model but we need to keep record for now. might change in the future.
		//$this->model_payment_ce_pay->pruneOutCall();
		$this->load->model('checkout/order');
		$this->load->model('setting/store');
		$query = $this->db->query("select a.* from ". DB_PREFIX ."cashenvoy_outcall a, `". DB_PREFIX ."order` b 
		where a.orderid=b.order_id and b.order_status_id<1 and (TIME_TO_SEC(timediff(now(),a.time))/60)>20 order by a.orderid");
		for($i=0;$i<count($query->rows);$i++){ 
			$transref = $query->rows[$i]['transref'];
			$request = 'mertid='.$this->config->get('ce_pay_mertid').'&transref='.$transref;
			$response = $this->requery($request);
			$response = strip_tags(preg_replace('#(<title.*?>).*?(</title>)#', '$1$2', $response)); 			
			$this->log->write('ce_pay :: Requery REQUEST: ' . $request);
			$this->log->write('ce_pay :: Requery RESPONSE: ' . $response);
			$respdata = explode('-',$response);
			$cnt = count($respdata);
			@$returned_transref = $respdata[0];
			@$returned_status = $respdata[1];
			@$returned_amount = $respdata[2];
			if ($cnt==3) {
				$order_status_id = $this->config->get('config_order_status_id');
				if((float)$query->rows[$i]['amount']!=$returned_amount && $returned_status=='C00') { 
					$returned_status='C05'; $a = 'Transaction amount '; 
				}
				switch($returned_status) {
					case 'C00':
						$order_status_id = $this->config->get('ce_pay_completed_status_id');
						$pfx = "CashEnvoy transaction successful. <br/>Reference: $transref"; break;
					case 'C04':
						$order_status_id = $this->config->get('ce_pay_denied_status_id');
						$pfx = "CashEnvoy transaction denied. <br/>Reason: Insufficient funds. <br/>Reference: $transref"; break;
					case 'C05':
						$order_status_id = $this->config->get('ce_pay_failed_status_id');
						$pfx = "CashEnvoy transaction failed. <br/>Reason: ".@$a."Error occurred. Contact support@cashenvoy.com <br/>
						Reference: $transref"; break;	
					case 'C03':
						$order_status_id = $this->config->get('ce_pay_failed_status_id');
						$pfx = "CashEnvoy transaction failed. <br/>Reason: No transaction record. <br/>Reference: $transref"; break;																	
				}
				if(@$returned_status){
					$this->model_checkout_order->addOrderHistory($query->rows[$i]['orderid'], $order_status_id, $pfx, TRUE);
				}
			}
		}
	}
	set_error_handler('error_handler'); 
	}
}
?>