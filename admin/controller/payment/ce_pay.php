<?php
class ControllerPaymentCEPay extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/ce_pay');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('ce_pay', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$data['heading_title'] = $this->language->get('heading_title');	
		$data['text_edit'] = $this->language->get('text_edit');		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['entry_mertid'] = $this->language->get('entry_mertid');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_key'] = $this->language->get('entry_key');
		$data['entry_total'] = $this->language->get('entry_total');	
		$data['entry_completed_status'] = $this->language->get('entry_completed_status');
		$data['entry_denied_status'] = $this->language->get('entry_denied_status');
		$data['entry_failed_status'] = $this->language->get('entry_failed_status'); 
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
 		if (isset($this->error['mertid'])) {
			$data['error_mertid'] = $this->error['mertid'];
		} else {
			$data['error_mertid'] = '';
		}		
		if (isset($this->error['total'])) {
			$data['error_total'] = $this->error['total'];
		} else {
			$data['error_total'] = '';
		}
		if (isset($this->error['key'])) {
			$data['error_key'] = $this->error['key'];
		} else {
			$data['error_key'] = '';
		}		
		$data['breadcrumbs'] = array();
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),      		
      		'separator' => false
   		);
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/ce_pay', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		$data['action'] = $this->url->link('payment/ce_pay', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		if (isset($this->request->post['ce_pay_mertid'])) {
			$data['ce_pay_mertid'] = $this->request->post['ce_pay_mertid'];
		} else {
			$data['ce_pay_mertid'] = $this->config->get('ce_pay_mertid');
		}
		if (isset($this->request->post['ce_pay_test'])) {
			$data['ce_pay_test'] = $this->request->post['ce_pay_test'];
		} else {
			$data['ce_pay_test'] = $this->config->get('ce_pay_test');
		}		
		if (isset($this->request->post['ce_pay_total'])) {
			$data['ce_pay_total'] = $this->request->post['ce_pay_total'];
		} else {
			$data['ce_pay_total'] = $this->config->get('ce_pay_total'); 
		} 	
		if (isset($this->request->post['ce_pay_completed_status_id'])) {
			$data['ce_pay_completed_status_id'] = $this->request->post['ce_pay_completed_status_id'];
		} else {
			$data['ce_pay_completed_status_id'] = $this->config->get('ce_pay_completed_status_id');
		}			
		if (isset($this->request->post['ce_pay_denied_status_id'])) {
			$data['ce_pay_denied_status_id'] = $this->request->post['ce_pay_denied_status_id'];
		} else {
			$data['ce_pay_denied_status_id'] = $this->config->get('ce_pay_denied_status_id');
		}		
		if (isset($this->request->post['ce_pay_failed_status_id'])) {
			$data['ce_pay_failed_status_id'] = $this->request->post['ce_pay_failed_status_id'];
		} else {
			$data['ce_pay_failed_status_id'] = $this->config->get('ce_pay_failed_status_id');
		}									
		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		if (isset($this->request->post['ce_pay_geo_zone_id'])) {
			$data['ce_pay_geo_zone_id'] = $this->request->post['ce_pay_geo_zone_id'];
		} else {
			$data['ce_pay_geo_zone_id'] = $this->config->get('ce_pay_geo_zone_id');
		}
		$this->load->model('localisation/geo_zone');
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		if (isset($this->request->post['ce_pay_status'])) {
			$data['ce_pay_status'] = $this->request->post['ce_pay_status'];
		} else {
			$data['ce_pay_status'] = $this->config->get('ce_pay_status');
		}		
		if (isset($this->request->post['ce_pay_sort_order'])) {
			$data['ce_pay_sort_order'] = $this->request->post['ce_pay_sort_order'];
		} else {
			$data['ce_pay_sort_order'] = $this->config->get('ce_pay_sort_order');
		}
		if (isset($this->request->post['ce_pay_key'])) {
			$data['ce_pay_key'] = $this->request->post['ce_pay_key'];
		} else {
			$data['ce_pay_key'] = $this->config->get('ce_pay_key');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('payment/ce_pay.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/ce_pay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->request->post['ce_pay_mertid'] || !ctype_digit($this->request->post['ce_pay_mertid'])) {
			$this->error['mertid'] = $this->language->get('error_mertid');
		}		
		if ($this->request->post['ce_pay_total']<50 || !ctype_digit($this->request->post['ce_pay_total'])) {
			$this->error['total'] = $this->language->get('error_total');
		}	
		if (strlen($this->request->post['ce_pay_key'])!=32) {
			$this->error['key'] = $this->language->get('error_key');
		}		
		return !$this->error;
	}
}
?>