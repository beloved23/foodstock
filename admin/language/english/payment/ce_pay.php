<?php
// Heading
$_['heading_title']					 = 'CashEnvoy (Interswitch, Verve, Mastercard, Visacard, eTranzact, GTBank Internet Banking)'; 

// Text
$_['text_payment']					 = 'Payment';
$_['text_success']					 = 'Success: You have modified CashEnvoy account details!';
$_['text_edit']                      = 'Edit CashEnvoy';
$_['text_ce_pay']				     = '<a onclick="window.open(\'https://www.cashenvoy.com\');"><img src="view/image/payment/cashenvoy.png" alt="CashEnvoy" title="CashEnvoy" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_mertid']					 = 'Merchant Id:';
$_['entry_key']						 = 'Secret Key:';
$_['entry_test']					 = 'Sandbox Mode:';
$_['entry_total']                    = 'Minimum Checkout Total';
$_['entry_completed_status']         = 'Completed Status:';
$_['entry_denied_status']			 = 'Denied Status:';
$_['entry_failed_status']			 = 'Failed Status:';
$_['entry_geo_zone']				 = 'Geo Zone:';
$_['entry_status']					 = 'Status:';
$_['entry_sort_order']				 = 'Sort Order:';

// Error
$_['error_permission']				 = 'Warning: You do not have permission to modify payment CashEnvoy!';
$_['error_mertid']					 = 'Merchant Id is required! Must be a number.';
$_['error_total']					 = 'Total cannot be less than 50.';
$_['error_key']						 = 'Secret key length must be 32 characters.';
?>