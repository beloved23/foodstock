<?php
// HTTP
define('HTTP_SERVER', 'http://127.0.0.1/cart/admin/');
define('HTTP_CATALOG', 'http://127.0.0.1/cart/');

// HTTPS
define('HTTPS_SERVER', 'http://127.0.0.1/cart/admin/');
define('HTTPS_CATALOG', 'http://127.0.0.1/cart/');

// DIR
define('DIR_APPLICATION', '/Applications/XAMPP/xamppfiles/htdocs/cart/admin/');
define('DIR_SYSTEM', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/');
define('DIR_LANGUAGE', '/Applications/XAMPP/xamppfiles/htdocs/cart/admin/language/');
define('DIR_TEMPLATE', '/Applications/XAMPP/xamppfiles/htdocs/cart/admin/view/template/');
define('DIR_CONFIG', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/config/');
define('DIR_IMAGE', '/Applications/XAMPP/xamppfiles/htdocs/cart/image/');
define('DIR_CACHE', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/storage/cache/');
define('DIR_DOWNLOAD', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/storage/download/');
define('DIR_LOGS', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/storage/logs/');
define('DIR_MODIFICATION', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/storage/modification/');
define('DIR_UPLOAD', '/Applications/XAMPP/xamppfiles/htdocs/cart/system/storage/upload/');
define('DIR_CATALOG', '/Applications/XAMPP/xamppfiles/htdocs/cart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'foo');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
